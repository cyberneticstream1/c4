export default function Page(){

    return(
            <>
            {/* SMALL */}
            <div className={"fixed bottom-0 w-11/12 bg-slate-300 opacity-50 h-80 left-1/2 -translate-x-1/2 rounded-xl md:hidden"}>
            </div>

            {/* MEDIUM */}
            <div className={"hidden md:block bottom-1/2 fixed opacity-50 left-3/4 translate-y-1/2 -translate-x-1/2 w-96 h-96 bg-slate-300 rounded-xl"}>
            </div>

            </>
    )



}